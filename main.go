package main

import (
	"fmt"
	"os"

	"gitea.com/lunny/pot/cmd"

	_ "github.com/shurcooL/vfsgen"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var (
	version = "0.0.2"

	rootCmd = &cobra.Command{
		Use: "pot",
	}

	verCmd = &cobra.Command{
		Use: "version",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println(version)
		},
	}
)

func main() {
	log.SetLevel(log.DebugLevel)

	rootCmd.AddCommand(
		verCmd,
		cmd.CmdRun,
	)

	if err := rootCmd.Execute(); err != nil {
		log.Fatalf("Failed to run app with %s: %v", os.Args, err)
	}
}
