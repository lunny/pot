package runtime

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseTaskCmd(t *testing.T) {
	var kases = []struct {
		RowCmd        string
		ExpectCmd     string
		ExpectArgs    []string
		ExpectArgsMap map[string]string
	}{
		{
			RowCmd:     `screen -name=1 -cmd="./abc -124"`,
			ExpectCmd:  "screen",
			ExpectArgs: []string{"-name=1", `-cmd="./abc -124"`},
			ExpectArgsMap: map[string]string{
				"name": "1",
				"cmd":  "./abc -124",
			},
		},
		{
			RowCmd:     `screen -name=1`,
			ExpectCmd:  "screen",
			ExpectArgs: []string{"-name=1"},
			ExpectArgsMap: map[string]string{
				"name": "1",
			},
		},
		{
			RowCmd:     `screen -name=1 -name2="2"`,
			ExpectCmd:  "screen",
			ExpectArgs: []string{"-name=1", `-name2="2"`},
			ExpectArgsMap: map[string]string{
				"name":  "1",
				"name2": "2",
			},
		},
		{
			RowCmd:     `screen -name=1 -name2=2`,
			ExpectCmd:  "screen",
			ExpectArgs: []string{"-name=1", `-name2=2`},
			ExpectArgsMap: map[string]string{
				"name":  "1",
				"name2": "2",
			},
		},
		{
			RowCmd:        `screen`,
			ExpectCmd:     "screen",
			ExpectArgs:    nil,
			ExpectArgsMap: map[string]string{},
		},
		{
			RowCmd:     `screen -names=["a", "b"]`,
			ExpectCmd:  "screen",
			ExpectArgs: []string{`-names=["a", "b"]`},
			ExpectArgsMap: map[string]string{
				"names": `["a", "b"]`,
			},
		},
		{
			RowCmd:     `screen -names=["a b"]`,
			ExpectCmd:  "screen",
			ExpectArgs: []string{`-names=["a b"]`},
			ExpectArgsMap: map[string]string{
				"names": `["a b"]`,
			},
		},
	}

	for _, kase := range kases {
		t.Run(kase.RowCmd, func(t *testing.T) {
			cmd, args, err := splitTaskCmd(kase.RowCmd)
			assert.NoError(t, err)
			assert.EqualValues(t, kase.ExpectCmd, cmd)
			assert.EqualValues(t, kase.ExpectArgs, args)

			cmd, argsMap, err := parseTaskCmd(kase.RowCmd)
			assert.NoError(t, err)
			assert.EqualValues(t, kase.ExpectCmd, cmd)
			assert.EqualValues(t, kase.ExpectArgsMap, argsMap)
		})
	}
}
