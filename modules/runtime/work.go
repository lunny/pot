package runtime

type Work struct {
	host string
	task *Task
	args []string
}

func (w *Work) Run() error {
	return w.task.Run(w.host, w.args...)
}
