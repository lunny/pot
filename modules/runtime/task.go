package runtime

import (
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
)

// Task represents a task on a module
type Task struct {
	Name  string
	Args  []string `yaml:"args"`
	Steps []Step   `yaml:"steps"`
	Pkg   *Package `yaml:"-"`
}

// Run run a task's steps
func (task *Task) Run(host string, args ...string) error {
	log.Infof("[%s] run task %s", host, task.Name)
	ctx, err := NewContext(task.Pkg, host, log.StandardLogger().Out)
	if err != nil {
		return err
	}
	return task.RunWithContext(ctx, args...)
}

// RunWithContext run task with derived ctx
func (task *Task) RunWithContext(ctx *Context, args ...string) error {
	if err := ctx.Login(); err != nil {
		return err
	}

	var allowArgs = make(map[string]bool)
	var data = make(map[string]interface{})
	// init default value
	for _, arg := range task.Args {
		parts := strings.SplitN(arg, "=", 2)
		if len(parts) == 2 {
			data[parts[0]] = parts[1]
		}
		allowArgs[parts[0]] = true
	}
	// param value will override default
	for _, field := range args {
		field = strings.TrimPrefix(field, "-")
		parts := strings.SplitN(field, "=", 2)
		if _, ok := allowArgs[parts[0]]; !ok {
			return fmt.Errorf("Unknow arg: %v", parts[0])
		}
		if len(parts) > 1 {
			data[parts[0]] = parts[1]
		} else {
			data[parts[0]] = ""
		}
	}

	for _, step := range task.Steps {
		if err := step.Run(ctx, data); err != nil {
			return err
		}
	}
	return nil
}
