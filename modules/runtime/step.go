package runtime

import (
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
	"text/template"
	"time"
)

// Step represents a task step
type Step struct {
	For  string // for for statement
	Type string // task, if blank it means remote shell
	Cmd  string
}

// UnmarshalYAML implements yaml.Unmarshaler interface
func (step *Step) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var cmd string
	if err := unmarshal(&cmd); err == nil {
		step.Cmd = cmd
		return nil
	}

	var m = make(map[string]string)
	if err := unmarshal(&m); err != nil {
		return err
	}
	if len(m) == 0 {
		return errors.New("No command found on step")
	}
	for tp, cmd := range m {
		tp = strings.ToLower(strings.TrimSpace(tp))
		if tp == "for" {
			step.For = strings.TrimSpace(cmd)
		} else {
			step.Type = tp
			step.Cmd = strings.TrimSpace(cmd)
			break
		}
	}

	return nil
}

// ExplainCmd render the commond with args
func (step *Step) ExplainCmd(args map[string]interface{}) (string, error) {
	tmpl, err := template.New("step").Parse(step.Cmd)
	if err != nil {
		return "", err
	}

	var bd strings.Builder
	if err := tmpl.Execute(&bd, args); err != nil {
		return "", err
	}

	return bd.String(), nil
}

// parseSlice parse ["a", "b"] to a slice
func parseSlice(s string) ([]string, error) {
	if !strings.HasPrefix(s, "[") || !strings.HasSuffix(s, "]") {
		return nil, errors.New("Slice format is incorrect")
	}
	fields := strings.Split(s[1:len(s)-1], ",")
	var res = make([]string, 0, len(fields))
	for _, field := range fields {
		field = strings.TrimSpace(field)
		if strings.HasPrefix(field, `"`) && strings.HasSuffix(field, `"`) {
			res = append(res, field[1:len(field)-1])
		} else {
			res = append(res, field)
		}
	}
	return res, nil
}

// Run executes a step
func (step *Step) Run(ctx *Context, args map[string]interface{}) error {
	if _, ok := args["host"]; !ok {
		args["host"] = ctx.host
	}
	if _, ok := args["user"]; !ok {
		args["user"] = ctx.user
	}
	args["now"] = time.Now()

	for k, v := range ctx.args {
		if _, ok := args[k]; !ok {
			args[k] = v
		}
	}

	var forV []string
	var forName string
	if step.For != "" {
		fields := strings.Fields(step.For)
		if len(fields) != 3 || fields[1] != "in" {
			return errors.New("for condition should be 'name in {{.names}}'")
		}

		forName = strings.TrimSpace(fields[0])
		var slice string
		if strings.HasPrefix(fields[2], "{{") && strings.HasSuffix(fields[2], "}}") {
			k := strings.TrimPrefix(strings.TrimSpace(fields[2][2:len(fields[2])-2]), ".")
			v, ok := args[k]
			if !ok {
				return fmt.Errorf("Unknow variable: %s", k)
			}
			slice = v.(string)
		} else {
			slice = fields[2]
		}
		var err error
		forV, err = parseSlice(slice)
		if err != nil {
			return err
		}
	} else {
		forV = []string{"1"}
	}

	var tp, varName string
	parts := strings.Split(step.Type, "(")
	tp = parts[0]
	if len(parts) > 1 {
		varName = strings.TrimSuffix(parts[1], ")")
	}

	for _, v := range forV {
		if forName != "" {
			args[forName] = v
		}

		realCmd, err := step.ExplainCmd(args)
		if err != nil {
			return err
		}

		switch tp {
		case "task":
			fields := strings.Fields(realCmd)
			if len(fields) == 0 {
				return errors.New("Command is empty")
			}
			task, err := ctx.pkg.FindTask(fields[0])
			if err != nil {
				return err
			}

			var newCtx = *ctx
			if err := task.RunWithContext(&newCtx, fields[1:]...); err != nil {
				return err
			}
			if varName != "" {
				ctx.lastArg = newCtx.lastArg
				ctx.args[varName] = ctx.lastArg
			}
		case "local":
			return errors.New("not supported")
		case "pot":
			potCmd, args, err := splitTaskCmd(realCmd)
			if err != nil {
				return err
			}
			switch potCmd {
			case "upload":
				if len(args) != 2 {
					return fmt.Errorf("upload needs two args")
				}
				if err := ctx.Upload(args[0], args[1]); err != nil {
					return err
				}
			case "download":
				if len(args) != 2 {
					return fmt.Errorf("download needs two args")
				}
				if err := ctx.Download(args[0], args[1]); err != nil {
					return err
				}
			case "list_dir":
				if len(args) != 1 {
					return fmt.Errorf("list_dir needs one args")
				}
				f, err := os.Open(args[0])
				if err != nil {
					return err
				}
				defer f.Close()
				files, err := f.Readdir(0)
				if err != nil {
					return err
				}
				var output = "["
				for i, file := range files {
					output += `"` + file.Name() + `"`
					if i != len(files)-1 {
						output += ","
					}
				}
				output += "]"
				fmt.Fprintln(ctx.output, output)
				if varName != "" {
					ctx.lastArg = output
					ctx.args[varName] = output
				}
			default:
				return fmt.Errorf("Unsupported pot command: -%s-, -%#v-", potCmd, args)
			}
		default:
			if varName == "" {
				if err := ctx.Execute(realCmd); err != nil {
					return err
				}
			} else {
				var builder strings.Builder
				if err := ctx.ExecuteWithOutput(realCmd, io.MultiWriter(ctx.output, &builder)); err != nil {
					return err
				}
				ctx.lastArg = builder.String()
				ctx.args[varName] = ctx.lastArg
			}
		}
	}
	return nil
}

func splitTaskCmd(cmd string) (string, []string, error) {
	parts := strings.SplitN(cmd, " ", 2)
	if len(parts) == 0 {
		return "", nil, errors.New("Command is blank")
	} else if len(parts) == 1 {
		return parts[0], nil, nil
	}
	cmd = strings.TrimSpace(parts[1])

	var (
		inQuoteMark, inBrackets, inSpace bool
		args                             = make([]string, 0, 10)
		start                            int
	)
	for i, c := range cmd {
		if c != ' ' && inSpace {
			start = i
			inSpace = false
		}

		switch c {
		case ' ':
			if !inSpace && !inQuoteMark && !inBrackets {
				args = append(args, cmd[start:i])
				inSpace = true
			}
		case '[':
			inBrackets = true
		case ']':
			inBrackets = false
		case '"':
			inQuoteMark = !inQuoteMark
		}

		if i == len(cmd)-1 && c != ' ' {
			args = append(args, cmd[start:i+1])
		}
	}
	return parts[0], args, nil
}

func trimQuote(s string) string {
	if strings.HasPrefix(s, `"`) && strings.HasSuffix(s, `"`) {
		return s[1 : len(s)-1]
	}
	return s
}

func parseFlag(arg string) (string, string) {
	arg = strings.TrimPrefix(arg, "-")
	parts := strings.SplitN(arg, "=", 2)
	if len(parts) == 1 {
		return parts[0], ""
	}
	return parts[0], trimQuote(parts[1])
}

func parseTaskCmd(cmd string) (string, map[string]string, error) {
	cmd, args, err := splitTaskCmd(cmd)
	if err != nil {
		return "", nil, err
	}
	var result = make(map[string]string)
	for _, arg := range args {
		k, v := parseFlag(arg)
		result[k] = v
	}
	return cmd, result, nil
}
