package runtime

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"path"
	"path/filepath"
	"strings"
	"sync"

	"gitea.com/lunny/pot/modules/packages"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v3"
)

// Package represents a package
type Package struct {
	Version     string   `yaml:"version"`
	Name        string   `yaml:"name"`
	Imports     []string `yaml:"imports"`
	Tasks       []Task   `yaml:"tasks"`
	importTasks []Task   `yaml:"-"`
	Main        struct {
		Hosts []string `yaml:"hosts"`
		Tasks []string `yaml:"tasks"`
	} `yaml:"main"`
}

type PackageType string

const (
	LocalPackageType  PackageType = "local"
	StdPackageType    PackageType = "std"
	RemotePackageType PackageType = "remote"
)

func packageType(p string) PackageType {
	if strings.HasPrefix(p, "./") {
		return LocalPackageType
	}
	fields := strings.Split(p, "/")
	if strings.Contains(fields[0], ".") {
		return RemotePackageType
	}
	return StdPackageType
}

// NewPackageFromFile load a package from file
func NewPackageFromFile(fPath string) (*Package, error) {
	bs, err := ioutil.ReadFile(fPath)
	if err != nil {
		return nil, err
	}

	return NewPackageFromBytes(filepath.Dir(fPath), bs)
}

func loadPackage(curDir, pkgPath string) (*Package, error) {
	tp := packageType(pkgPath)
	switch tp {
	case LocalPackageType:
		subPkgPath := filepath.Join(curDir, pkgPath+".yml")
		return NewPackageFromFile(subPkgPath)
	case StdPackageType:
		bs, err := packages.GetPackage(pkgPath)
		if err != nil {
			return nil, err
		}
		return NewPackageFromBytes("", bs)
	case RemotePackageType:
		return NewPackageFromRemote(pkgPath)
	default:
		return nil, errors.New("Unsupported")
	}
}

// NewPackageFromBytes loads a package from file contents
func NewPackageFromBytes(dir string, bs []byte) (*Package, error) {
	var pkg Package
	if err := yaml.Unmarshal(bs, &pkg); err != nil {
		return nil, err
	}

	for _, imp := range pkg.Imports {
		fields := strings.Fields(imp)
		if len(fields) == 1 {
			subPkg, err := loadPackage(dir, fields[0])
			if err != nil {
				return nil, err
			}

			pkg.importTasks = append(pkg.importTasks, subPkg.Tasks...)
		} else if len(fields) == 3 && fields[1] == "from" {
			subPkg, err := loadPackage(dir, fields[2])
			if err != nil {
				return nil, err
			}
			if err != nil {
				return nil, err
			}

			var found bool
			for _, t := range subPkg.Tasks {
				if t.Name == fields[0] {
					pkg.importTasks = append(pkg.importTasks, t)
					found = true
					break
				}
			}
			if !found {
				return nil, fmt.Errorf("load %s failed", imp)
			}
		}
	}

	return &pkg, nil
}

// NewPackageFromRemote load package from remote url
func NewPackageFromRemote(pkgPath string) (*Package, error) {
	parts := strings.Split(pkgPath, "/")
	var u string
	switch parts[0] {
	case "github.com":
		// https://github.com/lunny/tango/raw/master/public/cert.pem
		u = "https://" + path.Join(parts[0], parts[1], parts[2]) + "/raw/master/" + strings.Join(parts[3:], "/") + ".yml"
	case "gitea.com":
		// https://gitea.com/lunny/pot/raw/branch/master/examples/echo/echo.yml
		u = "https://" + path.Join(parts[0], parts[1], parts[2]) + "/raw/branch/master/" + strings.Join(parts[3:], "/") + ".yml"
	default:
		return nil, fmt.Errorf("Unsupported remote package: %s", pkgPath)
	}

	resp, err := http.Get(u)
	if err != nil {
		return nil, err
	}
	bs, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return nil, err
	}
	return NewPackageFromBytes("", bs)
}

type ErrTaskNotExist struct {
	TaskName string
}

func (err ErrTaskNotExist) Error() string {
	return fmt.Sprintf("%s is not exist", err.TaskName)
}

func IsErrTaskNotExist(err error) bool {
	_, ok := err.(ErrTaskNotExist)
	return ok
}

// FindTask find a task from the package
func (pkg *Package) FindTask(taskname string) (*Task, error) {
	for _, t := range pkg.Tasks {
		if t.Name == taskname {
			return &t, nil
		}
	}

	for _, t := range pkg.importTasks {
		if t.Name == taskname {
			return &t, nil
		}
	}
	return nil, ErrTaskNotExist{taskname}
}

// Run the package with special hosts and tasks
func (pkg *Package) Run(hosts []string, tasks []string) error {
	if len(hosts) == 0 {
		hosts = pkg.Main.Hosts
	}
	if len(hosts) == 0 {
		return errors.New("A runnable package must have a main section")
	}

	if len(tasks) == 0 {
		tasks = pkg.Main.Tasks
	}
	if len(tasks) == 0 {
		return errors.New("No tasks to run")
	}

	log.Infof("Run package %s with %v", pkg.Name, hosts)

	var hostTasks = make(map[string][]*Work)

	for _, taskName := range tasks {
		t, args, err := splitTaskCmd(taskName)
		if err != nil {
			return err
		}

		task, err := pkg.FindTask(t)
		if err != nil {
			if IsErrTaskNotExist(err) {
				log.Warnf("No task named %s", taskName)
				continue
			}
			return err
		}
		task.Pkg = pkg
		for _, host := range hosts {
			hostTasks[host] = append(hostTasks[host], &Work{
				host: host,
				task: task,
				args: args,
			})
		}
	}

	var wg sync.WaitGroup
	for _, works := range hostTasks {
		wg.Add(1)
		go func(works []*Work) {
			for _, work := range works {
				if err := work.Run(); err != nil {
					log.Errorf("[%s] run task %s failed: %v", work.host,
						work.task.Name, err)
					break
				} else {
					log.Infof("[%s] run task %s successfully.", work.host,
						work.task.Name)
				}
			}
			wg.Done()
		}(works)
	}
	wg.Wait()
	return nil
}
