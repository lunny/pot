// +build bindata

package packages

import "io/ioutil"

// GetPackage get std package content
func GetPackage(name string) ([]byte, error) {
	f, err := Assets.Open("/" + name + ".yml")
	if err != nil {
		return nil, err
	}
	defer f.Close()
	return ioutil.ReadAll(f)
}
