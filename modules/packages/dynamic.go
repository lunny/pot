// +build !bindata

package packages

import (
	"io/ioutil"
	"os"
)

// GetPackage get std package content
func GetPackage(name string) ([]byte, error) {
	f, err := os.Open("../../std/" + name + ".yml")
	if err != nil {
		return nil, err
	}
	defer f.Close()
	return ioutil.ReadAll(f)
}
