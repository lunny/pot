package cmd

import (
	"strings"

	"gitea.com/lunny/pot/modules/runtime"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var (
	CmdRun = &cobra.Command{
		Use: "run",
		Run: runRun,
	}

	Hosts string
	Tasks string
)

func init() {
	CmdRun.Flags().StringVarP(&Hosts, "hosts", "", "", "Comma seperated hosts")
	CmdRun.Flags().StringVarP(&Tasks, "tasks", "t", "", "Comma seperated tasks")
}

func runRun(cmd *cobra.Command, args []string) {
	if len(args) == 0 {
		log.Fatal("You must specify a package to run")
	}

	entryPkg := args[0]
	if entryPkg == "" {
		entryPkg = "./pot.yml"
	}

	pkg, err := runtime.NewPackageFromFile(entryPkg)
	if err != nil {
		log.Fatal(err)
	}

	var hosts, tasks []string
	if len(Hosts) > 0 {
		hosts = strings.Split(Hosts, ",")
	}
	if len(Tasks) > 0 {
		tasks = strings.Split(Tasks, ",")
	}

	if err := pkg.Run(hosts, tasks); err != nil {
		log.Fatal(err)
	}
}
