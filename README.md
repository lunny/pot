# pot - A tool to maintain remote machines

[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT) [![Release](https://raster.shields.io/badge/dynamic/json.svg?label=release&url=https://gitea.com/api/v1/repos/lunny/pot/releases&query=$[0].tag_name)](https://gitea.com/lunny/pot/releases) [![Build Status](https://drone.gitea.com/api/badges/lunny/pot/status.svg)](https://drone.gitea.com/lunny/pot) [![Go Report Card](https://goreportcard.com/badge/gitea.com/lunny/pot)](https://goreportcard.com/report/gitea.com/lunny/pot) [![GoDoc](https://godoc.org/gitea.com/lunny/pot?status.svg)](https://godoc.org/gitea.com/lunny/pot)

## Introduce

Pot is a tool to maintain many remote machines, it's inspired by ansible but easier
to learn. It's only one binary on local machine. Nothing is required for the target machines if your commands are only some scripts.

It maybe changed frequently. DON'T use it in production.

## Installation

```
go get gitea.com/lunny/pot
```

## Write a package ping.yml

```yaml
version: 1
name: ping the remote machine
tasks:
  - name: ping
    description: ping the machine from all the hosts
    args:
    - host # if host is empty, then it will keep the gloabl default value - current host
    - times=5
    steps:
    - ping -c {{.times}} {{.host}}
```

## Write a runnable pot.yml

A runnable pot.yml which is also a package but have a section named `main` with 
hosts and tasks.

```yaml
version: 1
name: ping the remote machine
imports:
  - ping from ping

main:
  hosts:
  - root@192.168.1.10:22022
  tasks:
  - ping
```

## Run with main

```
pot run pot.yml
```

## Run with no main

```
pot run pot.yml --hosts=root@192.168.1.10:22022 --tasks=ping
```

## Global variables

- {{.host}} The current machine
- {{.user}} Login user on current machine